
--- Required libraries.
local crypto
local json = require( "json" )
local lfs = require( "lfs" )
local mime

if system.getInfo( "platform" ) == "html5" then
	local base64 = require( ( _G.scrappyDir or "" ) .. "data.base64" )
	mime =
	{
		b64 = base64.encode,
		unb64 = base64.decode
	}
	local md5 = require( ( _G.scrappyDir or "" ) .. "language.md5" )
	crypto =
	{
		digest = function( _, string ) return md5.sum( string ) end
	}
else
	if system.getInfo( "platform" ) == "nx64" then
		local base64 = require( ( _G.scrappyDir or "" ) .. "data.base64" )
		mime =
		{
			b64 = base64.encode,
			unb64 = base64.decode
		}
	else
		mime = require( "mime" )
	end
	crypto = require( "crypto" )
end

-- Localised functions.
local open = io.open
local close = io.close
local read = io.read
local popen = os.popen
local remove = os.remove
local execute = os.execute
local lines = io.lines
local find = string.find
local gmatch = string.gmatch
local gsub = string.gsub
local match = string.match
local gsub = string.gsub
local format = string.format
local digest = crypto.digest
local encode = json.encode
local decode = json.decode
local insert = table.insert
local pathForFile = system.pathForFile
local mkdir = lfs.mkdir
local chdir = lfs.chdir
local dir = lfs.dir
local attributes = lfs.attributes
local getInfo = system.getInfo

function dirtree( dir )
    assert(dir and dir ~= "", "Please pass directory parameter")
    if string.sub(dir, -1) == "/" then
        dir=string.sub(dir, 1, -2)
    end

    local function yieldtree(dir)
        for entry in lfs.dir(dir) do
            if entry ~= "." and entry ~= ".." then
                entry=dir.."/"..entry
                local attr=lfs.attributes(entry)
                coroutine.yield(entry,attr)
                if attr.mode == "directory" then
                    yieldtree(entry)
                end
            end
        end
    end

    return coroutine.wrap(function() yieldtree(dir) end)
end

-- Localised values.
local md5 = crypto.md5
local ResourceDirectory = system.ResourceDirectory
local TemporaryDirectory = system.TemporaryDirectory

--- Class creation.
local library = {}

function library:init( params )

	self._params = params or {}

	self._languages = {}
	self._supported = self._params.supported

	-- Store the default language
	self._default = self._params.default or self._supported[ 1 ]

	-- Set the base dir for building the languages
	self._buildBaseDir = self._params.buildBaseDir or ResourceDirectory

	self._format = self._params.format or "json"
	self._directory = self._params.directory

	self._identifier = "|"
	self._arg = "<>"
	self._pattern = "(%b" .. self._identifier .. self._identifier .. ")"

	-- Set up some default file excludes
	self._excludes =
	{
		".git",
		".DS_Store",
		".CoronaLiveBuild",
		"README.md",
		"build.settings",
		"config.lua",
		".png",
		".jpg",
		".jpeg",
		".gif",
		".webp",
		".psd",
		".svg",
		".tiff",
		".ico",
		".icns",
		".wav",
		".ogg",
		".mp3",
		".opus",
		".mp4",
		".mpeg",
		".ttf",
		".otf",
		".nib",
		".storyboard",
		".plist",
		".scrappyLanguage",
		".scrappyData",
		"LICENSE",
		".xml",
		".nmeta",
		".src",
		"/language/core.lua",
		"\\language\\core.lua",
		"/random/core.lua",
		"\\random\\core.lua",
		"/jumper/core/heuristics.lua",
		"\\jumper\\core\\heuristics.lua",
		"/jumper/core/node.lua",
		"\\jumper\\core\\node.lua",
		"/jumper/pathfinder.lua",
		"\\jumper\\pathfinder.lua",
		"/jumper/grid.lua",
		"\\jumper\\grid.lua"
	}

	-- Set up some default file excludes
	self._includes =
	{
		".lua",
		".json",
		".config",
		".cfg"
	}

	-- Loop through any passed in excludes and add them to our list
	for i = 1, #( self._params.excludes or {} ), 1 do
		self._excludes[ #self._excludes + 1 ] = self._params.excludes[ i ]
	end

	-- Loop through any passed in includes and add them to our list
	for i = 1, #( self._params.includes or {} ), 1 do
		self._includes[ #self._includes + 1 ] = self._params.includes[ i ]
	end

	-- Have we got a set directory? But we're not on a device
	if self._directory and not system.getInfo( "environment" ) == "device" then

		-- Try to create the path
		local path = pathForFile( "", self._buildBaseDir )

		-- Did we?
		if path then

			-- Try to change to it
			local success, err = chdir( path )

			-- Did we?
			if success then

				-- Create the temp one for building
				mkdir( self._directory .. "/" )

			end

		else

		end

	end

	-- Set the current language to the default, or the initial if it's been passed in
	self:change( self._params.initial or self._default )

	-- Should we rebuild on start? Assuming we're in the simulator
	if self._params.rebuild and getInfo( "environment" ) == "simulator" then
		self:build()
	end

end

--- Checks if a language has been loaded.
-- @param name The name of the language.
-- @return True if it has been, false otherwise.
function library:isLanguageLoaded( name )
	return self._languages[ name ] ~= nil
end

--- Loads a language file.
-- @param name The name of the language.
-- @return True if it was loaded, false otherwise.
function library:loadLanguage( name )

	-- Is this language not loaded?
	if not self:isLanguageLoaded( name ) then

		-- Pre declare the index
		local index

		-- Loop through each supported language
		for i = 1, #( self._supported or {} ), 1 do

			-- Is this the one we want?
			if self._supported[ i ] == name then

				-- Get the index and breakout!
				index = i

				break

			end

		end

		-- Do we have a language?
		if index then

			-- Get the path to the language file
			local path = pathForFile( ( self._directory and self._directory .. "/" or "" ) .. self._supported[ index ] .. ".scrappyLanguage", ResourceDirectory )

			if path then

				-- Open the file for writing
				local file, errorString = open( path, "r" )

				-- Do we have a file?
				if file then

					-- Create the langauge table
					self._languages[ self._supported[ index ] ] = {}

					-- Are we using csv
					if self._format == "csv" then

						-- Loop through each line
						for line in lines( path ) do

							-- Table to store the line fields
							local fields = {}

							-- Split the line into two fields
							gsub( line, format( "([^%s]+)", ":" ), function( c ) fields[ #fields + 1 ] = c end )

							if #fields == 2 then

								-- Store out the line by using the hash as a key, remembering to remove the trailing comma
								self._languages[ self._supported[ index ] ][ gsub( fields[ 2 ], ",", "" ) ] = fields[ 1 ]

							end

						end

					-- Are we using json?
					elseif self._format == "json" then

						-- Get the contents
						local contents = read( file, "*a" )

						-- Read in the language
						self._languages[ self._supported[ index ] ] = decode( contents )

					end

					-- Close the file
					close( file )

					return true

				end

			end

		end

	end

end

--- Gets a translated string from the database.
-- @param name The name of the string.
-- @param language The name of the language.
-- @return The string.
function library:get( text, language, ... )

	-- Ensure we have some text
	if text then

		local string = text

		-- Check we have a language
		if self._languages[ language or self:current() ] then

			-- Remove the identifiers from the string
			string = gsub( string, self._identifier, "" )

			-- Get the found string, or the passed in text if not found
			string = self._languages[ language or self:current() ][ digest( md5, string ) ] or text

			-- Do we have some args passed in to swap out?
			if arg then

				-- Loop through each arg
				for i = 1, #arg, 1 do

					-- And find the next instance of `<>` to replace
					string = gsub( string, self._arg, function() return arg[ i ] end, 1 )

				end

			end

			-- And return the string
			return gsub( string, self._identifier, "" ) or text

		end

		return text

	end

	return text

end

--- Formats a string and pulls out the identifier char.
-- @param s The string to format.
-- @param ... The args as per string.format.
-- @return The string.
function library:format( s, ... )
	return gsub( format( s, ... ), self._identifier, "" )
end

--- Sets the current language.
-- @param name The name of the language.
function library:change( name )

	-- Is the language supported?
	if self:isSupported( name ) then

		-- Set it
		self._current = name

		-- Load it if required
		self:loadLanguage( name )

		-- And return true
		return true

	end

end

--- Gets the current language.
-- @return The langauge name.
function library:current()
	return self._current
end

--- Gets a list of all languages.
-- @return The names.
function library:list()

	-- Table to store the names
	local names = {}

	-- Loop through the current languages
	for k, _ in pairs( self._languages ) do

		-- Store the name
		names[ #names + 1 ] = k

	end

	-- Return the names
	return names

end

--- Builds all language files.
function library:build()

	-- Loop through each file in the language directory
	for filename, attr in dirtree( pathForFile( self._directory, ResourceDirectory ) ) do

		-- And remove it
		remove( filename )

	end

	-- Get a list of all files in the current root directory
	local files = self:_getFiles( pathForFile( "", ResourceDirectory ) )

	-- Table to store all the actual hashes created for this build
	local hashes = {}

	-- Loop through each filename
	for i = 1, #files, 1 do

		-- Get the text from the file
		local text = self:_readFile( files[ i ] )

		-- Get all the translatable strings
		local strings = self:_parseText( text )

		-- Loop through all the strings
		for j = 1, #strings, 1 do

			-- Loop through all the supported languages
			for k = 1, #self._supported, 1 do

				-- Ensure we have a table for the language
				self._languages[ self._supported[ k ] ] = self._languages[ self._supported[ k ] ] or {}

				-- Make sure we have a table for this language's new hashes
				hashes[ self._supported[ k ] ] = hashes[ self._supported[ k ] ] or {}

				-- Are we dealing with the primary language?
				if self._supported[ k ] == self._default then

					-- Store them out with the hash as the name and plain text as the value
					self._languages[ self._supported[ k ] ][ strings[ j ].hash ] = strings[ j ].text

					-- Store this hash out so we know we've just added it
					hashes[ self._supported[ k ] ][ strings[ j ].hash ] = true

				else

					-- Store them out with the hash as the name and plain text as the value assuming the value hasn't changed
					self._languages[ self._supported[ k ] ][ strings[ j ].hash ] = self._languages[ self._supported[ k ] ][ strings[ j ].hash ] or strings[ j ].text

					-- Store this hash out so we know we've just added it
					hashes[ self._supported[ k ] ][ strings[ j ].hash ] = true

				end

			end

		end

	end

	-- Loop through all the languages
	for name, strings in pairs( self._languages ) do

		-- Now loop through this language's hashes
		for hash, string in pairs( strings ) do

			-- Check if this hash is one that we've just added
			if not hashes[ name ][ hash ] then

				-- If it's not then it's redundant so goodbye!
				self._languages[ name ][ hash ] = nil

			end

		end

	end

	-- Loop through all the supported languages
	for i = 1, #( self._supported or {} ), 1 do

		-- Create a path for the langauge files
		local path = pathForFile( ( self._directory and self._directory .. "/" ) or "", self._buildBaseDir )

		if path then
			path = path .. self._supported[ i ] .. ".scrappyLanguage"
		end

		-- Remove any existing file
		remove( path )

		-- Open the file for writing
		local file, errorString = open( path, "w" )

		-- Check we have a file
		if file then

			if self._format == "csv" then

				-- Loop through all the strings
				for k, v in pairs( self._languages[ self._supported[ i ] ] or {} ) do

					-- Write out the string
					file:write( v .. ":" .. k .. ",\n" )

				end

			elseif self._format == "json" then

				-- Write out the language
				file:write( encode( self._languages[ self._supported[ i ] ] ) )

			end

			-- Close the file handle
			close( file )

		end

	end

end

--- Check if a language is supported.
-- @param name The name of the language.
-- @return True if it is, false otherewise.
function library:isSupported( name )

	-- Loop through each supported language
	for i = 1, #( self._supported or {} ), 1 do

		-- Does this match the passed in code?
		if self._supported[ i ] == name then

			-- It's supported
			return true

		end

	end

	-- Not supported
	return false

end

--- Check if a file is safe. I.e. it's not in our ignore list.
-- @param file The filename.
-- @return True if it is, false otherewise.
function library:_fileIsSafe( file )

	-- Loop through all our excludes
	for i = 1, #( self._excludes or {} ), 1 do

		-- Check if this file matches an exclude
		if find( file, self._excludes[ i ] ) then

			-- It's not safe
			return false

		end

	end

	-- Loop through all our includes
	for i = 1, #( self._includes or {} ), 1 do

		-- Check if this file matches an include
		if find( file, self._includes[ i ] ) then

			-- It is safe!
			return true

		end

	end

	-- It's not safe
	return false

end

--- Gets a list of all files in a path, recursively.
-- @param path The path.
-- @return The filenames.
function library:_getFiles( path )

	-- Get the directory seperator for the current platform
	local dirSeperator = package.config:sub( 1, 1 )

	-- Function to get files recursively
	function listFiles( path, files )

		-- Sse provided list or create a new one
		files = files or {}

		-- Loop through each entry in the path
		for entry in dir( path ) do

			-- Ensure we're not special . entry
			if entry ~= "." and entry ~= ".." then

				-- Get the file path
				local file = path .. dirSeperator .. entry

				-- Is it a directory?
				if attributes( file ).mode == 'directory' then

					-- Loop through this path as well
					listFiles( file, files )

				-- We're not a directory, so must be a file. But is it a safe file?
				elseif self:_fileIsSafe( file ) then

					-- It is so insert the file into our list
					insert( files, file )

				end

			end

		end

		-- Return the files
		return files

	end

	-- Get and return the list of files
	return listFiles( path )

end

--- Reads text from a file.
-- @param path The path to the file.
-- @return The file contents.
function library:_readFile( path )

	-- Check we have a path
	if path then

		-- Open the file for reading
		local file, errorString = open( path, "r" )

		if file then

			-- Get the contents
			local contents = file:read( "*a" )

			-- Close it out
			close( file )

			-- return the contents
			return contents

		end

	end
end

--- Parses some text and extracts any translatable strings.
-- @param text The text to parse.
-- @return A list of strings, each being a table with 'text' and 'hash'.
function library:_parseText( text )

	-- Table for the strings
	local strings = {}

	-- Do we have some text
	if text then

		-- Loop through each match of our special pattern
		for s in gmatch( text, self._pattern ) do

			-- Remove the identifier from the string
			s = s:gsub( self._identifier, "" )

			-- Add the entry, making sure to hash it as well
			strings[ #strings + 1 ] = { text = s, hash = digest( md5, s ) }


		end

	end

	-- Return the strings
	return strings

end

-- If we don't have a global Scrappy object i.e. this is the first Scrappy plugin to be included
if not Scrappy then

	-- Create one
	Scrappy = {}

end

-- If we don't have a Scrappy Language library
if not Scrappy.Language then

	-- Then store the library out
	Scrappy.Language = library

end

-- Return the new library
return library
